import telebot
from telebot import types
import requests
from currency_converter import CurrencyConverter



token = '6814490444:AAE50TW2KEUwAQ8PtkB0icTRH9FEzNP0Yow'



bot = telebot.TeleBot('6814490444:AAE50TW2KEUwAQ8PtkB0icTRH9FEzNP0Yow')

# @bot.message_handler(commands=['start', 'hello', 'main', 'help'])
# def main(message):
#     bot.send_message(message.chat.id, f'Привет! {message.from_user.first_name} {message.from_user.username}' )


# @bot.message_handler(commands=['help'])
# def main(message):
#     bot.send_message(message.chat.id, '<b>Help<b> <em><u>information<u><em>') 

# @bot.message_handler(commands=['start'])
# def start(message):
#     markup = types.ReplyKeyboardMarkup()
#     btn1 = types.InlineKeyboardButton('Перейти на сайт')
#     markup.row(btn1)
#     btn2 = types.KeyboardButton('Удалить фото')
#     btn3 = types.KeyboardButton('Изменить')
#     markup.row(btn2,btn3)
#     bot.send_message(message.chat.id, 'Привет', reply_markup=markup)
#     bot.register_next_step_handler(message, on_click)

# def on_click(message):
#     if message.text == 'Перейти на сайт':
#         bot.send_message(message.chat.id,'website is open')
#     elif message.text == 'Удалить фото':
#         bot.send_message(message.chat.id,'delete')

# @bot.message_handler(content_types=['photo'])
# def get_photo(message):
#     markup = types.InlineKeyboardMarkup()
#     btn1 = types.InlineKeyboardButton('Перейти на сайт', url='https://google.com')
#     markup.row(btn1)
#     btn2 = types.InlineKeyboardButton('Удалить фото', callback_data='delete')
#     btn3 = types.InlineKeyboardButton('Изменить', callback_data='edit')
#     markup.row(btn2,btn3)


#     bot.reply_to(message, 'Ваше фото',reply_markup=markup)


# @bot.callback_query_handler(func=lambda callback: True)
# def callback_message(callback):
#     if callback.data == 'delete':
#         bot.delete_message(callback.message.chat.id, callback.message.message_id -1)
#     elif callback.data == 'edit':
#         bot.edit_message_text('Edit text', callback.message.chat.id, callback.message.message_id


currency = CurrencyConverter()
amount = 0

@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, "Приветствую, введите сумму для конвертации")
    bot.register_next_step_handler(message,summa)


def summa(message):
    global amount
    try:
        amount = int(message.text.strip())
    except ValueError:
        bot.send_message(message.chat.id, 'Введите корректное число!')
        bot.register_next_step_handler(message, summa)
        return

    if amount > 0:
        markup = types.InlineKeyboardMarkup(row_width=2)
        btn1 = types.InlineKeyboardButton('USD/EUR', callback_data='usd/eur')
        btn2 = types.InlineKeyboardButton('EUR/USD', callback_data='eur/usd')
        btn3 = types.InlineKeyboardButton('USD/GPB', callback_data='usd/gpb')
        btn4 = types.InlineKeyboardButton('другое значение', callback_data='else')
        markup.add(btn1, btn2, btn3, btn4)
        bot.send_message(message.chat.id, 'Выберите пару валют', reply_markup=markup)
    else:
        bot.send_message(message.chat.id, 'Введённое число не может быть отрицательным. Введитe вашу сумму')
        bot.register_next_step_handler(message, summa)


@bot.callback_query_handler(func=lambda call:True)
def callback(call):
    values = call.data.upper().split('/')
    res = currency.convert(amount, values[0], values[1])
    rework_res = round(res,2)
    bot.send_message(call.message.chat.id, f'Получается {rework_res}. Можете заново вводить число')
    bot.register_next_step_handler(call.message, summa)

bot.infinity_polling()